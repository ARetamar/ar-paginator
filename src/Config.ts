let angularJs: angular.IAngularStatic = (window as any).angular;
let APP: ng.IModule = angularJs.module("ar-paginator", []);

APP.component("paginator", PaginatorComponent);
APP.service("PaginatorService", PaginatorService);
