/// <reference types="angular" />
/// <reference types="angular-material" />

class PaginatorController implements ng.IController {
  public static $inject: string[] = ["$scope", "$log"];
  private data: any = {};
  private pages: number;
  private startPage: number = 1;
  private onPageChange: any;

  constructor(
    private Scope: ng.IScope,
    private Log: ng.ILogService,
  ) {}

  public $onInit() {
    this.data = {
      currentPage: this.startPage  || 1,
      pages: this.pages,
      startPage: this.startPage,
    };

    if (this.data.startPage !== 1) {
      this.goToPage(this.data.startPage);
    }
  }

  public goToPreviousPage(): void {
    this.goToPage(this.data.currentPage - 1);
  }

  public goToNextPage(): void {
    this.goToPage(this.data.currentPage + 1);
  }

  public goToPage(page: number): void {
    this.onPageChange({ page });
    this.data.currentPage = page;
  }

  public range(count: number): any {
    let ratings = [];
    for (let i = 1; i < count; i++) {
      ratings.push(i);
    }
    return ratings;

  }
}

let PaginatorComponent: ng.IComponentOptions = {
  bindings: {
    onPageChange: "&",
    pages: "=",
    startPage: "=",
  },
  controller: PaginatorController,
  template: `<div layout="row" style="margin: 0 auto;">
                <md-button class="md-raised md-fab" ng-click="$ctrl.goToPreviousPage();" ng-disabled="$ctrl.data.currentPage === 1">Prev</md-button>
                <md-button class="md-raised md-fab" ng-repeat="n in $ctrl.range($ctrl.pages)" ng-click="$ctrl.goToPage(n);" ng-class="{ 'md-primary' : $ctrl.data.currentPage === n }">{{n}}</md-button>
                <md-button class="md-raised md-fab" ng-click="$ctrl.goToNextPage();" ng-disabled="$ctrl.data.currentPage === ($ctrl.data.pages - 1)"> Next </md-button>
              </div>`,
};
