"use strict";

/**
 * Packages.
 */
const gulp = require("gulp");
const concat = require("gulp-concat");
const rename = require("gulp-rename");
const uglify = require("gulp-uglify");
const tsc = require("gulp-typescript");
const tslint = require("gulp-tslint");
const pkg = require("./package.json");

/**
 * Configuration.
 */
const DIST_DIR = "dist";
const SRC_DIR = "src";

const tsProject = tsc.createProject("tsconfig.json");

/**
 * Compile task for ts.
 */
gulp.task("compile", ["lint"], () => {
  let tsResult = gulp.src([SRC_DIR + "/Paginator.ts", SRC_DIR + "/PaginatorService.ts", SRC_DIR + "/Config.ts"])
    .pipe(tsProject());
  return tsResult.js
    .pipe(concat("paginator.js"))
    .pipe(gulp.dest(DIST_DIR))
    .pipe(rename("paginator.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest(DIST_DIR))
});

gulp.task("lint", () => {
  return gulp.src([SRC_DIR + "/**/*.ts"])
    .pipe(tslint({ formatter: "verbose" }))
    .pipe(tslint.report());
});


/**
 * Content task.
 */
gulp.task("content", () => {
  return gulp.src(["app/**/*",
      "!**/*.ts",
      "!**/*.scss",
      "!**/*.html",
      "!app/images/*",
      // "!app/*.json",
    ], { nodir: true })
    .pipe(gulp.dest(DIST_DIR));
});
