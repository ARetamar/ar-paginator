/// <reference types="angular" />
/// <reference types="angular-material" />
var PaginatorController = (function () {
    function PaginatorController(Scope, Log) {
        this.Scope = Scope;
        this.Log = Log;
        this.data = {};
        this.startPage = 1;
    }
    PaginatorController.prototype.$onInit = function () {
        this.data = {
            currentPage: this.startPage || 1,
            pages: this.pages,
            startPage: this.startPage,
        };
        if (this.data.startPage !== 1) {
            this.goToPage(this.data.startPage);
        }
    };
    PaginatorController.prototype.goToPreviousPage = function () {
        this.goToPage(this.data.currentPage - 1);
    };
    PaginatorController.prototype.goToNextPage = function () {
        this.goToPage(this.data.currentPage + 1);
    };
    PaginatorController.prototype.goToPage = function (page) {
        this.onPageChange({ page: page });
        this.data.currentPage = page;
    };
    PaginatorController.prototype.range = function (count) {
        var ratings = [];
        for (var i = 1; i < count; i++) {
            ratings.push(i);
        }
        return ratings;
    };
    return PaginatorController;
}());
PaginatorController.$inject = ["$scope", "$log"];
var PaginatorComponent = {
    bindings: {
        onPageChange: "&",
        pages: "=",
        startPage: "=",
    },
    controller: PaginatorController,
    template: "<div layout=\"row\" style=\"margin: 0 auto;\">\n                <md-button class=\"md-raised md-fab\" ng-click=\"$ctrl.goToPreviousPage();\" ng-disabled=\"$ctrl.data.currentPage === 1\">Prev</md-button>\n                <md-button class=\"md-raised md-fab\" ng-repeat=\"n in $ctrl.range($ctrl.pages)\" ng-click=\"$ctrl.goToPage(n);\" ng-class=\"{ 'md-primary' : $ctrl.data.currentPage === n }\">{{n}}</md-button>\n                <md-button class=\"md-raised md-fab\" ng-click=\"$ctrl.goToNextPage();\" ng-disabled=\"$ctrl.data.currentPage === ($ctrl.data.pages - 1)\"> Next </md-button>\n              </div>",
};

var PaginatorService = (function () {
    function PaginatorService() {
    }
    PaginatorService.prototype.sum = function (num1, num2) {
        return num1 + num2;
    };
    return PaginatorService;
}());

var angularJs = window.angular;
var APP = angularJs.module("ar-paginator", []);
APP.component("paginator", PaginatorComponent);
APP.service("PaginatorService", PaginatorService);
